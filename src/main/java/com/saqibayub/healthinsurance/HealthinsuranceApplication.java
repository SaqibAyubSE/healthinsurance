package com.saqibayub.healthinsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class HealthinsuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthinsuranceApplication.class, args);
	}
}
