package com.saqibayub.healthinsurance.controller.quote;

import java.util.List;

public class BasicInformation {
	
	String name;
//	List<Gender> genders;
	int gender; //1 Male 0 FEMALE
	int age;
	List<HealthCheck> healthChecks;
	List<Habbit> habbits;
	
	Double basePremium;
	Double insuranceAmount;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public List<HealthCheck> getHealthChecks() {
		return healthChecks;
	}
	public void setHealthChecks(List<HealthCheck> healthChecks) {
		this.healthChecks = healthChecks;
	}

	public List<Habbit> getHabbits() {
		return habbits;
	}
	public void setHabbits(List<Habbit> habbits) {
		this.habbits = habbits;
	}

	public Double getBasePremium() {
		return basePremium;
	}
	public void setBasePremium(Double basePremium) {
		this.basePremium = basePremium;
	}
	public Double getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(Double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	
}
