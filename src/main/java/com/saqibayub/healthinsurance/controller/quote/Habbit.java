package com.saqibayub.healthinsurance.controller.quote;

public class Habbit {
	
	int habbitId;
	String habbitName;
	boolean impact;
	int positiveImpact;
	int negativeImpact;
	
	public int getHabbitId() {
		return habbitId;
	}
	public void setHabbitId(int habbitId) {
		this.habbitId = habbitId;
	}
	public String getHabbitName() {
		return habbitName;
	}
	public void setHabbitName(String habbitName) {
		this.habbitName = habbitName;
	}
	public boolean isImpact() {
		return impact;
	}
	public void setImpact(boolean impact) {
		this.impact = impact;
	}
	public int getPositiveImpact() {
		return positiveImpact;
	}
	public void setPositiveImpact(int positiveImpact) {
		this.positiveImpact = positiveImpact;
	}
	public int getNegativeImpact() {
		return negativeImpact;
	}
	public void setNegativeImpact(int negativeImpact) {
		this.negativeImpact = negativeImpact;
	}

	
}
