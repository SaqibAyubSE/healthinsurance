package com.saqibayub.healthinsurance.controller.quote;


public class HealthCheck {
	
	int healthCheckId;
	String healthCheckName;
	boolean impact;
	int positiveImpact;
	int negativeImpact;

	public int getHealthCheckId() {
		return healthCheckId;
	}
	public void setHealthCheckId(int healthCheckId) {
		this.healthCheckId = healthCheckId;
	}
	public String getHealthCheckName() {
		return healthCheckName;
	}
	public void setHealthCheckName(String healthCheckName) {
		this.healthCheckName = healthCheckName;
	}
	public boolean isImpact() {
		return impact;
	}
	public void setImpact(boolean impact) {
		this.impact = impact;
	}
	public int getPositiveImpact() {
		return positiveImpact;
	}
	public void setPositiveImpact(int positiveImpact) {
		this.positiveImpact = positiveImpact;
	}
	public int getNegativeImpact() {
		return negativeImpact;
	}
	public void setNegativeImpact(int negativeImpact) {
		this.negativeImpact = negativeImpact;
	}
}
