package com.saqibayub.healthinsurance.controller.quote;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.saqibayub.healthinsurance.repository.quote.HabbitRepository;
import com.saqibayub.healthinsurance.repository.quote.HealthCheckRepository;
import com.saqibayub.healthinsurance.service.quote.PremiumService;
import com.saqibayub.healthinsurance.service.quote.Utility;

@Controller
public class HealthInsuranceQuoteController {

	@Autowired
	HealthCheckRepository healthCheckRepository;
	@Autowired
	HabbitRepository habbitRepository;
	@Autowired
	PremiumService premiumService;
	@GetMapping(value="/quotes/initial-quote")
public String getMembers(ModelMap modalMap) {
		BasicInformation basicInformation = new BasicInformation();
		populateUI(basicInformation);
		
	 	modalMap.addAttribute("basicInformation", basicInformation);
	 	return "insurance-quote-calculator";
	}
	@PostMapping(value="quotes/initial-quote")
	public String getMembers(
			ModelMap modalMap,
			@Valid
			BasicInformation basicInformation, 
			BindingResult biningResult,
	        @RequestParam(value = "selectedHealthChecks") ArrayList<Integer> selectedHealthChecks,
	        @RequestParam(value = "selectedHabbits") ArrayList<Integer> selectedHabbits
			) {
		
	 	populateUI(basicInformation);
	 	
	 	selectedHealthChecks.forEach(checkedValue->{
	 		for(HealthCheck healthCheck : basicInformation.getHealthChecks()) {
	 			if(checkedValue.intValue()==healthCheck.getHealthCheckId()) {
	 				healthCheck.setImpact(true);
	 				break;
	 			}
	 		}
	 	});
	 	
	 	selectedHabbits.forEach(checkedValue->{
	 		for(Habbit habbit : basicInformation.getHabbits()) {
	 			if(checkedValue.intValue()==habbit.getHabbitId()) {
	 				habbit.setImpact(true);
	 				break;
	 			}
	 		}
	 	});
	 	com.saqibayub.healthinsurance.service.quote.BasicInformation bi=new com.saqibayub.healthinsurance.service.quote.BasicInformation();
	 	bi.setAge(basicInformation.getAge());
	 	bi.setGender(basicInformation.getGender());
	 	bi.setBasePremium(basicInformation.getBasePremium());
	 	//BeanUtils.copyProperties(basicInformation, bi);
	 	bi.setHabbits(new ArrayList<com.saqibayub.healthinsurance.service.quote.Habbit>());
	 	basicInformation.getHabbits().forEach(habbit->{
	 		com.saqibayub.healthinsurance.service.quote.Habbit h=
	 				new com.saqibayub.healthinsurance.service.quote.Habbit(habbit.getHabbitId(), habbit.getHabbitName(), habbit.isImpact(), habbit.getPositiveImpact(), habbit.getNegativeImpact()); 
//	 		BeanUtils.copyProperties(habbit, h);
	 		bi.getHabbits().add(h);
	 	});
	 	bi.setHealthChecks(new ArrayList<com.saqibayub.healthinsurance.service.quote.HealthCheck>());
	 	basicInformation.getHealthChecks().forEach(healthCheck->{
	 		com.saqibayub.healthinsurance.service.quote.HealthCheck h=
	 				new com.saqibayub.healthinsurance.service.quote.HealthCheck(healthCheck.getHealthCheckId(), healthCheck.getHealthCheckName(), healthCheck.isImpact(), healthCheck.getPositiveImpact(), healthCheck.getNegativeImpact()); 
//	 		BeanUtils.copyProperties(healthCheck, h);
	 		bi.getHealthChecks().add(h);
	 	});
	 	
	 	double value=0.00;
		try {
			value = premiumService.getPremiumAmount(bi);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		modalMap.addAttribute("calculatedAmount", value);
		return "result";
	}
	private void populateUI(BasicInformation basicInformation) {

		Iterable<com.saqibayub.healthinsurance.repository.quote.HealthCheck> repoHealthChecks = healthCheckRepository.findAll();
		Iterable<com.saqibayub.healthinsurance.repository.quote.Habbit> repoHabbits = habbitRepository.findAll();
		List<com.saqibayub.healthinsurance.controller.quote.HealthCheck> uiHealthChecks = 
				new ArrayList<com.saqibayub.healthinsurance.controller.quote.HealthCheck>();
		repoHealthChecks.forEach(heathCheck-> {
			com.saqibayub.healthinsurance.controller.quote.HealthCheck thisHealthCheck=new com.saqibayub.healthinsurance.controller.quote.HealthCheck();
			BeanUtils.copyProperties(heathCheck, thisHealthCheck);
			uiHealthChecks.add(thisHealthCheck);
			
		});
		basicInformation.setHealthChecks(uiHealthChecks);
		
		List<com.saqibayub.healthinsurance.controller.quote.Habbit> uiHabbits = 
				new ArrayList<com.saqibayub.healthinsurance.controller.quote.Habbit>();
		repoHabbits.forEach(heathCheck-> {
			com.saqibayub.healthinsurance.controller.quote.Habbit thisHabbit=new com.saqibayub.healthinsurance.controller.quote.Habbit();
			BeanUtils.copyProperties(heathCheck, thisHabbit);
			uiHabbits.add(thisHabbit);
			
		});
		basicInformation.setHabbits(uiHabbits);
	}
}