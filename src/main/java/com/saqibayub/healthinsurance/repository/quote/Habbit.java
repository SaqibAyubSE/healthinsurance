package com.saqibayub.healthinsurance.repository.quote;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="habbits")
public class Habbit {
	
	@Id
	@Column(name="habbit_id")
	@GeneratedValue
	Integer habbitId;
    
	@Column(name="habbit_name")
	String habbitName;
	@Column(name="positive_impact")
	int positiveImpact;
	@Column(name="negative_impact")
	int negativeImpact;

	public Habbit() {
		super();
	}
	public Habbit(int habbitId, String habbitName, int positiveImpact, int negativeImpact) {
		this();
		this.habbitId = habbitId;
		this.habbitName = habbitName;
		this.positiveImpact = positiveImpact;
		this.negativeImpact = negativeImpact;
	}
	public int getHabbitId() {
		return habbitId;
	}
	public void setHabbitId(int habbitId) {
		this.habbitId = habbitId;
	}
	public String getHabbitName() {
		return habbitName;
	}
	public void setHabbitName(String habbitName) {
		this.habbitName = habbitName;
	}

	public int getPositiveImpact() {
		return positiveImpact;
	}
	public void setPositiveImpact(int positiveImpact) {
		this.positiveImpact = positiveImpact;
	}
	public int getNegativeImpact() {
		return negativeImpact;
	}
	public void setNegativeImpact(int negativeImpact) {
		this.negativeImpact = negativeImpact;
	}

	
}
