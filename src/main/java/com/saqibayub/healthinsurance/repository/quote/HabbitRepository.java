package com.saqibayub.healthinsurance.repository.quote;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HabbitRepository extends CrudRepository<Habbit, Integer>{

}
