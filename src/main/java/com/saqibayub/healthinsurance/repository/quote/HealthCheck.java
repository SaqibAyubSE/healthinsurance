package com.saqibayub.healthinsurance.repository.quote;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="health_checks")
public class HealthCheck {

	@Id
	@GeneratedValue
	@Column(name="health_check_id")
	Integer healthCheckId;
	@Column(name="health_check_name")
	String healthCheckName;
	@Column(name="positive_impact")
	int positiveImpact;
	@Column(name="negative_impact")
	int negativeImpact;
	public HealthCheck() {
		super();
	}
	public HealthCheck(int healthCheckId, String healthCheckName, int positiveImpact, int negativeImpact) {
		this();
		this.healthCheckId = healthCheckId;
		this.healthCheckName = healthCheckName;
		this.positiveImpact = positiveImpact;
		this.negativeImpact = negativeImpact;
	}
	public int getHealthCheckId() {
		return healthCheckId;
	}
	public void setHealthCheckId(int healthCheckId) {
		this.healthCheckId = healthCheckId;
	}
	public String getHealthCheckName() {
		return healthCheckName;
	}
	public void setHealthCheckName(String healthCheckName) {
		this.healthCheckName = healthCheckName;
	}
	public int getPositiveImpact() {
		return positiveImpact;
	}
	public void setPositiveImpact(int positiveImpact) {
		this.positiveImpact = positiveImpact;
	}
	public int getNegativeImpact() {
		return negativeImpact;
	}
	public void setNegativeImpact(int negativeImpact) {
		this.negativeImpact = negativeImpact;
	}
}
