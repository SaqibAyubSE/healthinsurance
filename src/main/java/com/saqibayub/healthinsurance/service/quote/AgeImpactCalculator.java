package com.saqibayub.healthinsurance.service.quote;

import org.apache.logging.log4j.Logger;

public class AgeImpactCalculator extends ImpactCalculator 
{

	ImpactCalculator impactCalculator;
	int age;
	public AgeImpactCalculator(ImpactCalculator impactCalculator, int age) {
		this.impactCalculator = impactCalculator;
		this.age=age;
	}

	@Override
	public double calculate(double baseAmount) throws Exception {
		double otherImpact = this.impactCalculator.calculate(baseAmount);
		System.out.println("My cal culation on  : "+otherImpact);
		double thisImpact = 0.00;
		double totalImpact = 0.00;
		int loopCounter = (int)Math.floor((age-18)/5);
		if(loopCounter>0) {
			for(int i=0;i<loopCounter;i++) {
				thisImpact = getAgeImpact(otherImpact);
				otherImpact = otherImpact + thisImpact;
			}
		}
		
		return otherImpact;
	}

	private double getAgeImpact(double baseAmount) throws Exception {
		//% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% 
		//increase every 5 years
		int percentageAmount = getPercentageAmount();
		return Utility.calculatePercentage(baseAmount,percentageAmount);
		
	}
	public int getPercentageAmount() throws Exception {
		if(age==0) {
			throw new Exception("Age not Found");
		}
		int amountInPercentage=0; 
		if(this.age>18 && this.age<=25) {
			amountInPercentage=10;
		}else if(this.age>25 && this.age<=30) {
			amountInPercentage=10;
		}else if(this.age>30 && this.age<=35) {
			amountInPercentage=10;
		}else if(this.age>35 && this.age<=40) {
			amountInPercentage=10;
		}else if(this.age>40) {
			amountInPercentage=20;
		}
		return amountInPercentage;
	}
}
