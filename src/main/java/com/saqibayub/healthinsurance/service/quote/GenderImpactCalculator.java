package com.saqibayub.healthinsurance.service.quote;


public class GenderImpactCalculator extends ImpactCalculator 
{

	ImpactCalculator impactCalculator;
	int gender;
	public GenderImpactCalculator(ImpactCalculator impactCalculator, int gender) {
		this.impactCalculator = impactCalculator;
		this.gender=gender;
	}

	@Override
	public double calculate(double baseAmount) throws Exception {
		double otherImpact = this.impactCalculator.calculate(baseAmount);
		System.out.println("My cal culation on  : "+otherImpact);
		double thisImpact = getAgeImpact(otherImpact);

		double totalImpact = otherImpact + thisImpact;
		System.out.println("totalImpact : "+totalImpact +" : Gender  "+gender+" : "+ thisImpact);
		
		return totalImpact;
	}

	private double getAgeImpact(double baseAmount) throws Exception {
		//% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% 
		//increase every 5 years
		int percentageAmount = getPercentageAmount();
		return Utility.calculatePercentage(baseAmount,percentageAmount);
		
	}
	public int getPercentageAmount() throws Exception {
		int amountInPercentage=0;
		if(gender==1) {
			amountInPercentage=2;
		}
		return amountInPercentage;
	}
}
