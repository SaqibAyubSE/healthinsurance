package com.saqibayub.healthinsurance.service.quote;

import java.util.List;

public class HabbitImpactCalculator extends ImpactCalculator {

	ImpactCalculator impactCalculator;
	List<Habbit> habbits;
	public HabbitImpactCalculator(ImpactCalculator impactCalculator, List<Habbit> habbits) {
		this.impactCalculator = impactCalculator;
		this.habbits=habbits;
	}

	public double calculate(double baseAmount)throws Exception{
		double otherImpact = this.impactCalculator.calculate(baseAmount);
		System.out.println("My cal culation on  : "+otherImpact);
		double totalImpact = 0.00;
		
		for(Habbit habbit : this.habbits) {		
			double thisImpact = getAgeImpact(otherImpact,habbit);
			totalImpact += thisImpact;
			System.out.println(" Health "+habbit.getHabbitName()+" : "+habbit.isImpact()+" : "+ thisImpact);
		}
		totalImpact+=otherImpact;
		System.out.println("totalImpact : "+totalImpact);
		return totalImpact;
	}

	private double getAgeImpact(double baseAmount,Habbit habbit) throws Exception {
		//Good habits (Daily exercise) -> Reduce 3% for every good habit
		int percentageAmount = getPercentageAmount(habbit);
		return Utility.calculatePercentage(baseAmount,percentageAmount);
	}
	public int getPercentageAmount(Habbit habbit) throws Exception {
		int amountInPercentage = habbit.isImpact()?habbit.getPositiveImpact():habbit.getNegativeImpact();
		return amountInPercentage;
	}
}