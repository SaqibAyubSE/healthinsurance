package com.saqibayub.healthinsurance.service.quote;

import java.util.List;

public class HealthCheckImpactCalculator extends ImpactCalculator {

	ImpactCalculator impactCalculator;
	List<HealthCheck> healthChecks;
	public HealthCheckImpactCalculator(ImpactCalculator impactCalculator, List<HealthCheck> healthChecks) {
		this.impactCalculator = impactCalculator;
		this.healthChecks=healthChecks;
	}

	@Override
	public double calculate(double baseAmount) throws Exception {
		double otherImpact = this.impactCalculator.calculate(baseAmount);
//		double thisImpact = getAgeImpact(baseAmount);
		System.out.println("My cal culation on  : "+otherImpact);
		double totalImpact = 0.00;
		for(HealthCheck healthCheck:healthChecks){
			double thisImpact = getAgeImpact(otherImpact,healthCheck);
			totalImpact += thisImpact;
			System.out.println(" Health "+healthCheck.getHealthCheckName()+" : "+healthCheck.isImpact()+" : "+ thisImpact);
		}
		totalImpact = otherImpact+totalImpact;
		System.out.println("totalImpact : "+totalImpact );
		return totalImpact;
	}

	private double getAgeImpact(double baseAmount,HealthCheck healthCheck) throws Exception {
		// Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
		int percentageAmount = getPercentageAmount(healthCheck);
		return Utility.calculatePercentage(baseAmount,percentageAmount);
	}
	public int getPercentageAmount(HealthCheck healthCheck) throws Exception {
		int amountInPercentage = healthCheck.isImpact()?healthCheck.getPositiveImpact():healthCheck.getNegativeImpact();
		return amountInPercentage;
	}
}
