package com.saqibayub.healthinsurance.service.quote;

public class ImpactCalculator {
	
	public double calculate(double baseAmount)throws Exception{
		return baseAmount;
	}
	public static ImpactCalculator getInstance(BasicInformation bi)
	{
		ImpactCalculator iCalculator=new ImpactCalculator();
		
		iCalculator=new AgeImpactCalculator(iCalculator,bi.getAge());
		iCalculator=new GenderImpactCalculator(iCalculator,bi.getGender());
//		for(HealthCheck healthCheck:bi.getHealthChecks() )
//		{
			iCalculator=new HealthCheckImpactCalculator (iCalculator,bi.getHealthChecks());
//		}
//		for(Habbit habbit:bi.getHabbits()){
			iCalculator=new HabbitImpactCalculator(iCalculator,bi.getHabbits());
//		}
		return iCalculator;

	}
}
