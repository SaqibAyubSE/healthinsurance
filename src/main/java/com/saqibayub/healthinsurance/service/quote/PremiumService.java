package com.saqibayub.healthinsurance.service.quote;

import org.springframework.stereotype.Service;

@Service
public class PremiumService {

	public double getPremiumAmount(BasicInformation bi) throws Exception {
		
		ImpactCalculator iCalculator = ImpactCalculator.getInstance(bi);
		double calculatedAmount=iCalculator.calculate(bi.getBasePremium());
		return calculatedAmount;
	}

}
