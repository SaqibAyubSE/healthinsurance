package com.saqibayub.healthinsurance.service.quote;

public class Utility {

	public static double calculatePercentage(double baseAmount, int percentageAmount) {
		double value = baseAmount/100*percentageAmount;
		return value;
	}
}
