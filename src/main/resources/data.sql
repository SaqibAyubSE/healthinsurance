DROP TABLE IF EXISTS habbits;
DROP TABLE IF EXISTS health_checks;

CREATE TABLE `habbits` (
  `habbit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `habbit_name` VARCHAR(255) ,
  `positive_impact` INT(3) ,
  `negative_impact` INT(3) ,
  PRIMARY KEY (`habbit_id`)
) ;
CREATE TABLE `health_checks` (
  `health_check_id` INT(11) NOT NULL AUTO_INCREMENT,
  `health_check_name` VARCHAR(255) ,
  `positive_impact` INT(3) ,
  `negative_impact` INT(3) ,
  PRIMARY KEY (`health_check_id`)
);
