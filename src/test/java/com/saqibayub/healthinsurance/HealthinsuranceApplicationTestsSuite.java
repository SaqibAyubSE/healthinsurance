package com.saqibayub.healthinsurance;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.saqibayub.healthinsurance.repository.quote.HealthCheck;
import com.saqibayub.healthinsurance.service.quote.AgeImpactCalculatorTest;
import com.saqibayub.healthinsurance.service.quote.HabbitImpactCalculatorTest;
import com.saqibayub.healthinsurance.service.quote.HealthCheckImpactCalculatorTest;
import com.saqibayub.healthinsurance.service.quote.PremiumServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	AgeImpactCalculatorTest.class,
	HealthCheckImpactCalculatorTest.class,
	HabbitImpactCalculatorTest.class,
	PremiumServiceTest.class
})
public class HealthinsuranceApplicationTestsSuite {

	@Test
	public void contextLoads() {
	}

}
