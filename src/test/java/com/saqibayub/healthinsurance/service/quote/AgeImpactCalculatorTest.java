package com.saqibayub.healthinsurance.service.quote;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
public class AgeImpactCalculatorTest {

	private static final double premiumAmount=5000;

//	@Test
//	public void testPercentageByAge10() {
//		// <18
//		ImpactCalculator impactCalculator = new ImpactCalculator();
//		impactCalculator =new AgeImpactCalculator(impactCalculator,10);
//		double result=0.00;
//		try {
//			result = impactCalculator.calculate(premiumAmount);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		double expected = 0.00;
//		assertEquals(expected, result,0.01);
//	}
	@Test
	public void testPercentageByAge34() {
		// 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10%
		ImpactCalculator impactCalculator = new ImpactCalculator();
		impactCalculator =new AgeImpactCalculator(impactCalculator,34);
		double result=0.00;
		try {
			result = impactCalculator.calculate(premiumAmount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		double expected = 6655.00;
		assertEquals(expected, result,0.01);
	}
//	@Test
//	public void testPercentageByAge40() {
//		// 40+ -> 20%
//		ImpactCalculator impactCalculator = new ImpactCalculator();
//		impactCalculator =new AgeImpactCalculator(impactCalculator,41);
//		double result=0.00;
//		try {
//			result = impactCalculator.calculate(premiumAmount);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		double expected = 1000.00;
//		assertEquals(expected, result,0.01);
//	}
}
