package com.saqibayub.healthinsurance.service.quote;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
public class GenderImpactCalculatorTest {

	private static final double premiumAmount=6655;

	@Test
	public void testPercentageByMale() {
		// <18
		ImpactCalculator impactCalculator = new ImpactCalculator();
		impactCalculator =new GenderImpactCalculator(impactCalculator,1);
		double result=0.00;
		try {
			result = impactCalculator.calculate(premiumAmount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		double expected = 6788.1;
		assertEquals(expected, result,0.01);
	}
}
