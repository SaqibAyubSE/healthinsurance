package com.saqibayub.healthinsurance.service.quote;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
public class HabbitImpactCalculatorTest {


	private static final double premiumAmount=6855.981;

	@Test
	public void testGoodHabbitYes() {
		Habbit habbit3=new Habbit(3, "Daily exercise", true,-3, 0);
		List<Habbit> habbits=new ArrayList<Habbit>();
		habbits.add(habbit3);
		
		ImpactCalculator impactCalculator = new ImpactCalculator();
		impactCalculator =new HabbitImpactCalculator(impactCalculator,habbits);
		double result=0.00;
		try {
			result = impactCalculator.calculate(premiumAmount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		double expected = 6650.30157;
		assertEquals(expected, result,0.01);
	}
	@Test
	public void testBadHabbitYes() {
		Habbit habbit3=new Habbit(3, "Alcohol", true,3, 0);
		List<Habbit> habbits=new ArrayList<Habbit>();
		habbits.add(habbit3);
		
		ImpactCalculator impactCalculator = new ImpactCalculator();
		impactCalculator =new HabbitImpactCalculator(impactCalculator,habbits);
		double result=0.00;
		try {
			result = impactCalculator.calculate(6650.30157);
		} catch (Exception e) {
			e.printStackTrace();
		}
		double expected = 6849.8106171;
		assertEquals(expected, result,0.01);
	}
//	@Test
//	public void testBadHabbitYes() {
//		Habbit habbit4=new Habbit(4, "Drugs",true, 3, -3);
//		
//		ImpactCalculator impactCalculator = new ImpactCalculator();
//		impactCalculator =new HabbitImpactCalculator(impactCalculator,habbit4);
//		double result=0.00;
//		try {
//			result = impactCalculator.calculate(premiumAmount);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		double expected = 150.00;
//		assertEquals(expected, result,0.01);
//	}
//	@Test
//	public void testBadHabbitNo() {
//		
//		Habbit habbit4=new Habbit(4, "Drugs",false, 3, -3);
//		
//		ImpactCalculator impactCalculator = new ImpactCalculator();
//		impactCalculator =new HabbitImpactCalculator(impactCalculator,habbit4);
//		double result=0.00;
//		try {
//			result = impactCalculator.calculate(premiumAmount);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		double expected = -150.00;
//		assertEquals(expected, result,0.01);
//	}
}