package com.saqibayub.healthinsurance.service.quote;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class HealthCheckImpactCalculatorTest {

	private static final double premiumAmount=6788.1;

	@Test
	public void testHealthTestPositive() {
		//Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
		//Overweight: Yes
		HealthCheck heathCheck4=new HealthCheck(4, "Overweight", true,1, 0);
		List<HealthCheck> heathChecks=new ArrayList<HealthCheck>();
		heathChecks.add(heathCheck4);
		ImpactCalculator impactCalculator = new ImpactCalculator();
		impactCalculator =new HealthCheckImpactCalculator(impactCalculator,heathChecks);
		double result=0.00;
		try {
			result = impactCalculator.calculate(premiumAmount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		double expected = 6855.981;
		assertEquals(expected, result,0.01);
	}
	@Test
	public void testHealthTestNegative() {
		//Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
		//Overweight: Yes
		HealthCheck heathCheck4=new HealthCheck(4, "Overweight", true,-1, 1);
		List<HealthCheck> heathChecks=new ArrayList<HealthCheck>();
		heathChecks.add(heathCheck4);
		ImpactCalculator impactCalculator = new ImpactCalculator();
		impactCalculator =new HealthCheckImpactCalculator(impactCalculator,heathChecks);
		double result=0.00;
		try {
			result = impactCalculator.calculate(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		double expected = 4950.00;
		assertEquals(expected, result,0.01);
	}
}