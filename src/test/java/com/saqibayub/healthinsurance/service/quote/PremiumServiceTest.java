package com.saqibayub.healthinsurance.service.quote;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
public class PremiumServiceTest {

/*
Name: Norman Gomes
Gender: Male
Age: 34 years
Current health:


Hypertension: No
Blood pressure: No
Blood sugar: No
Overweight: Yes


Habits:

Smoking: No
Alcohol: Yes
Daily exercise: Yes
Drugs: No 
 */

		private static final double premiumAmount=5000;
		private static final BasicInformation basicInformation=new BasicInformation();
		private static final PremiumService premiumService=new PremiumService();
		@BeforeClass
		public static void init() {
			
			int gender = 1;
			int age = 34;
			List<HealthCheck> healthChecks=new ArrayList<HealthCheck>();
			HealthCheck heathCheck1=new HealthCheck(1, "Hypertension", false, 1, 0);
			HealthCheck heathCheck2=new HealthCheck(2, "Blood pressure", false, 1, 0);
			HealthCheck heathCheck3=new HealthCheck(3, "Blood sugar", false, 1, 0);
			HealthCheck heathCheck4=new HealthCheck(4, "Overweight", true, 1, 0);
			
			healthChecks.add(heathCheck1);
			healthChecks.add(heathCheck2);
			healthChecks.add(heathCheck3);
			healthChecks.add(heathCheck4);
			
			List<Habbit> habbits=new ArrayList<Habbit>();
			
			Habbit habbit1=new Habbit(1, "Smoking", false,3, 0);
			Habbit habbit2=new Habbit(2, "Alcohol", true, 3, 0);
			Habbit habbit3=new Habbit(3, "Daily exercise",true, -3, 0);
			Habbit habbit4=new Habbit(4, "Drugs",false, 3, 0);
			
			habbits.add(habbit1);
			habbits.add(habbit2);
			habbits.add(habbit3);
			habbits.add(habbit4);
			
			basicInformation.setBasePremium(premiumAmount);
			basicInformation.setAge(age);
			basicInformation.setGender(gender);
			basicInformation.setHealthChecks(healthChecks);
			basicInformation.setHabbits(habbits);
			
		}
		@Test
		public void testPremiumAmount() {

			
			double result=0.00;
			try {
				result = premiumService.getPremiumAmount(basicInformation);
			} catch (Exception e) {
				e.printStackTrace();
			}
			double expected = 6856.00;
			assertEquals(expected, result,0.1);
			
		}
	}